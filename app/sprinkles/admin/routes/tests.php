<?php

/*
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @copyright Copyright (c) 2019 Alexander Weissman
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/LICENSE.md (MIT License)
 */

use UserFrosting\Sprinkle\Core\Util\NoCache;

/*
 * Routes for administrative user management.
 */
$app->group('/company', function () {
    $this->get('', 'UserFrosting\Sprinkle\Admin\Controller\TestController:pageList')
        ->setName('uri_tests');

    $this->get('/u/{user_name}', 'UserFrosting\Sprinkle\Admin\Controller\TestController:pageInfo');
})->add('authGuard')->add(new NoCache());

$app->group('/api/company', function () {
    $this->delete('/u/{user_name}', 'UserFrosting\Sprinkle\Admin\Controller\TestController:delete');


    $this->get('/u/{user_name}', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getInfo');

    $this->get('/u/{user_name}/activities', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getActivities');

    $this->get('/u/{user_name}/roles', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getRoles');

    $this->get('/u/{user_name}/permissions', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getPermissions');

    $this->post('', 'UserFrosting\Sprinkle\Admin\Controller\TestController:create');

    $this->post('/u/{user_name}/password-reset', 'UserFrosting\Sprinkle\Admin\Controller\TestController:createPasswordReset');

    $this->put('/u/{id}', 'UserFrosting\Sprinkle\Admin\Controller\TestController:updateInfo');

    $this->put('/u/{user_name}/{field}', 'UserFrosting\Sprinkle\Admin\Controller\TestController:updateField');
})->add('authGuard')->add(new NoCache());

$app->group('/modals/company', function () {
    $this->get('/confirm-delete', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getModalConfirmDelete');

    $this->get('/create', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getModalCreate');

    $this->get('/edit', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getModalEdit');

    $this->get('/file', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getModalFile');

    $this->get('/password', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getModalEditPassword');

    $this->get('/roles', 'UserFrosting\Sprinkle\Admin\Controller\TestController:getModalEditRoles');
})->add('authGuard')->add(new NoCache());
