<?php

/*
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @copyright Copyright (c) 2019 Alexander Weissman
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/LICENSE.md (MIT License)
 */

namespace UserFrosting\Sprinkle\Admin\Controller;

use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use UserFrosting\Fortress\Adapter\JqueryValidationAdapter;
use UserFrosting\Fortress\RequestDataTransformer;
use UserFrosting\Fortress\RequestSchema;
use UserFrosting\Fortress\ServerSideValidator;
use UserFrosting\Sprinkle\Account\Database\Models\Test;
use UserFrosting\Sprinkle\Account\Database\Models\CompanyFile;
use UserFrosting\Sprinkle\Account\Facades\Password;
use UserFrosting\Sprinkle\Core\Controller\SimpleController;
use UserFrosting\Sprinkle\Core\Mail\EmailRecipient;
use UserFrosting\Sprinkle\Core\Mail\TwigMailMessage;
use UserFrosting\Support\Exception\BadRequestException;
use UserFrosting\Support\Exception\ForbiddenException;
use UserFrosting\Support\Exception\NotFoundException;
use DateTime;

/**
 * Controller class for user-related requests, including listing users, CRUD for users, etc.
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class TestController extends SimpleController
{
    /**
     * Processes the request to create a new user (from the admin controls).
     *
     * Processes the request from the user creation form, checking that:
     * 1. The username and email are not already in use;
     * 2. The logged-in user has the necessary permissions to update the posted field(s);
     * 3. The submitted data is valid.
     * This route requires authentication.
     *
     * Request type: POST
     *
     * @see getModalCreate
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function create(Request $request, Response $response, array $args)
    {
        // Get POST parameters: user_name, first_name, last_name, email, locale, (group)
        $params = $request->getParsedBody();

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        

        /** @var \UserFrosting\Sprinkle\Core\Alert\AlertStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/test/create.yaml');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Ensure that in the case of using a single locale, that the locale is set bu inheriting from current user
        if (count($this->ci->locale->getAvailableIdentifiers()) <= 1) {
            $data['locale'] = $currentUser->locale;
        }

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;


        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        // If currentUser does not have permission to set the group, but they try to set it to something other than their own group,
        // echo "<pre>"; print_r($data); echo "</pre>"; die('anil testing');
        // All checks passed!  log events/activities, create user, and send verification email (if required)
        // Begin transaction - DB will be rolled back if an exception occurs

        Capsule::transaction(function () use ($classMapper, $data, $ms, $config, $currentUser,$params) {
            // Create the user
            if (intval($params['id']) > 0) {
                // echo "<pre>"; print_r($params); echo "</pre>"; die('anil testing');
                $user =  $classMapper->getClassMapping('test')::where('id', $params['id'])
                ->first();
                
                // $user->name = $params['name'];
                // $user->email = $params['email'];
                // $user->xero_url = $params['xero_url'];
                // $user->secret = $params['secret'];
                // $user->password = $params['password'];
                $ms->addMessageTranslated('success', 'COMPANY_UPDATED', [
                    'name' => $user->name,
                ]);
            }else{
                $user = new Test();
                $params['name'] = json_encode($params['name'],true);
                // $user = $classMapper->createInstance('test', $data);
                $ms->addMessageTranslated('success', 'TEST.CREATED', $data);
            }
            foreach ($params as $name => $value) {
                if ($name != 'id' && $name != 'csrf_name' && $name != 'csrf_value') {
                    $user->$name = $value;
                }
            }

            // Store new user to database
            $user->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} created a new test.", [
                'type'    => 'test_create',
                'user_id' => $currentUser->id,
            ]);

            
        });
        return $response->withJson([], 200);
    }

    /**
     * Processes the request to send a user a password reset email.
     *
     * Processes the request from the user update form, checking that:
     * 1. The target user's new email address, if specified, is not already in use;
     * 2. The logged-in user has the necessary permissions to update the posted field(s);
     * 3. We're not trying to disable the master account;
     * 4. The submitted data is valid.
     * This route requires authentication.
     *
     * Request type: POST
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function createPasswordReset(Request $request, Response $response, array $args)
    {
        // Get the username from the URL
        $user = $this->getUserFromParams($args);

        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;


        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        /** @var \UserFrosting\Sprinkle\Core\Alert\AlertStream $ms */
        $ms = $this->ci->alerts;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction(function () use ($user, $config) {
            // Create a password reset and shoot off an email
            $passwordReset = $this->ci->repoPasswordReset->create($user, $config['password_reset.timeouts.reset']);

            // Create and send welcome email with password set link
            $message = new TwigMailMessage($this->ci->view, 'mail/password-reset.html.twig');

            $message->from($config['address_book.admin'])
                    ->addEmailRecipient(new EmailRecipient($user->email, $user->full_name))
                    ->addParams([
                        'user'         => $user,
                        'token'        => $passwordReset->getToken(),
                        'request_date' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);

            $this->ci->mailer->send($message);
        });

        $ms->addMessageTranslated('success', 'PASSWORD.FORGET.REQUEST_SENT', [
            'email' => $user->email,
        ]);

        return $response->withJson([], 200);
    }

    /**
     * Processes the request to delete an existing user.
     *
     * Deletes the specified user, removing any existing associations.
     * Before doing so, checks that:
     * 1. You are not trying to delete the master account;
     * 2. You have permission to delete the target user's account.
     * This route requires authentication (and should generally be limited to admins or the root user).
     *
     * Request type: DELETE
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException   If user is not found
     * @throws ForbiddenException  If user is not authorized to access page
     * @throws BadRequestException
     */
    public function delete(Request $request, Response $response, $id)
    {
        $classMapper = $this->ci->classMapper;

        // Get the user to delete
        $user = $classMapper->getClassMapping('test')::where('id', $id)
            ->first();

        // $user = $this->getUserFromParams($args);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        

        $userName = $user->name;

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction(function () use ($user, $userName, $currentUser) {
            $user->delete();
            unset($user);

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} deleted the account for {$userName}.", [
                'type'    => 'account_delete',
                'user_id' => $currentUser->id,
            ]);
        });

        /** @var \UserFrosting\Sprinkle\Core\Alert\AlertStream $ms */
        $ms = $this->ci->alerts;

        $ms->addMessageTranslated('success', 'DELETION_COMPANY', [
            'user_name' => $userName,
        ]);

        return $response->withJson([], 200);
    }

    /**
     * Returns activity history for a single user.
     *
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getActivities(Request $request, Response $response, array $args)
    {
        $user = $this->getUserFromParams($args);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // GET parameters
        $params = $request->getQueryParams();

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        

        $sprunje = $classMapper->createInstance('activity_sprunje', $classMapper, $params);

        $sprunje->extendQuery(function ($query) use ($user) {
            return $query->where('user_id', $user->id);
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Returns info for a single user.
     *
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getInfo(Request $request, Response $response, array $args)
    {
        $user = $this->getUserFromParams($args);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Join user's most recent activity
        $user = $classMapper->createInstance('user')
                            ->where('user_name', $user->user_name)
                            ->joinLastActivity()
                            ->with('lastActivity', 'group')
                            ->first();

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        

        $result = $user->toArray();

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $response->withJson($result, 200, JSON_PRETTY_PRINT);
    }

    /**
     * Returns a list of Users.
     *
     * Generates a list of users, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws ForbiddenException If user is not authorized to access page
     */
     public function getCompanyList(Request $request){
        // echo "<pre>"; print_r(json_decode(CompanyFile::where('id',1)->first()->zip,true)); echo "</pre>";
        // echo "<pre>"; print_r(json_decode(CompanyFile::where('id',1)->first()->screen,true)); echo "</pre>";
        //  die('anil testing');
        // $base64String = json_decode(CompanyFile::where('id',1)->first()->zip,true)[0];

        // // Decode the base64 string to binary data
        // $binaryString = base64_decode($base64String);

        // // Set the response headers to indicate it's a zip file download
        // header('Content-Type: application/zip');
        // header('Content-Disposition: attachment; filename="example.zip"');

        // // Output the binary data
        // echo $binaryString;
        $params = $request->getQueryParams();
        if($params['key'] !== '6$cd[ZU9Gq$:!a[4'){
            return json_encode([
                'success' => false,
                'message' => 'Unauthorized.'
            ],401);
        }
        $b = CompanyFile::get()->pluck('company_name')->toArray();
        $b = array_unique($b);
        $b = array_merge($b,["Botanic Stores","Naylor & Devlin Limited 2023","Eurotec Safety Systems","Botanic Stores","Seearo Group Limited","SWEETBRIAR ADVISORY LTD","Loweco Ltd"]);
        // echo "<pre>"; print_r($b); echo "</pre>"; die('anil testing');
        if (isset($params['id'])) {
            $allcreds = Test::whereIn('id',explode(',', $params['id']))->get();
        }else{
            $allcreds = Test::all();
        }
        
        
        $creds = [];
        foreach ($allcreds as $key => $value) {
            $value->DATES_one_yr = date('d-m-Y',strtotime($value->date_range2_end));

            $date = new DateTime($value->date_range2_end); // Replace with your desired date
            $date->modify('-1 year');

            // Format the modified date as a string (optional)
            $prevDate = $date->format('d-m-Y');
            $date->modify('+2 year');
            $nextDate = $date->format('d-m-Y');
            $value->DATES = ([$prevDate,$value->DATES_one_yr,$nextDate]);
            $value->DATES_for_reports_3yr  = ([
                [date('d-m-Y',strtotime($value->date_range1_start)),date('d-m-Y',strtotime($value->date_range1_end))],
                [date('d-m-Y',strtotime($value->date_range2_start)),date('d-m-Y',strtotime($value->date_range2_end))],
                [date('d-m-Y',strtotime($value->date_range3_start)),date('d-m-Y',strtotime($value->date_range3_end))],
            ]);
            $value->payroll_dates_act = ([date('d-m-Y',strtotime($value->payroll_dates_act1)),date('d-m-Y',strtotime($value->payroll_dates_act2))]);
            $value->payroll_dates_sum = ([date('d-m-Y',strtotime($value->payroll_dates_sum1)),date('d-m-Y',strtotime($value->payroll_dates_sum2))]);
            $a = json_decode($value->name,true);
            $result_c = array_diff($a, $b);
            if (count($result_c) > 0) {
            // if (true) {
                $creds[] = [
                    'id'=>$value->id,
                    'xero_url'=>$value->xero_url,
                    'email'=>$value->email,
                    'password'=>$value->password,
                    'company_name'=>$a,
                    'q1'=>$value->q1,
                    'q2'=>$value->q2,
                    'q3'=>$value->q3,
                    'q4'=>$value->q4,
                    'q5'=>$value->q5,
                    'q6'=>$value->q6,
                    'q7'=>$value->q7,
                    'q8'=>$value->q8,
                    'q9'=>$value->q9,
                    'q10'=>$value->q10,
                    'secret'=>$value->secret,
                    'DATES_one_yr'=>$value->DATES_one_yr,
                    'DATES'=>$value->DATES,
                    'DATES_for_reports_3yr'=>$value->DATES_for_reports_3yr,
                    'start_long_date'=>date('d-m-Y',strtotime($value->start_long_date)),
                    'end_long_date'=>date('d-m-Y',strtotime($value->end_long_date)),
                    'start_date'=>date('d-m-Y',strtotime($value->start_date)),
                    'end_date'=>date('d-m-Y',strtotime($value->end_date)),
                    'start_date_for_vat'=>date('d-m-Y',strtotime($value->start_date_for_vat)),
                    'end_date_for_vat'=>date('d-m-Y',strtotime($value->end_date_for_vat)),
                    'payroll_dates_act'=>$value->payroll_dates_act,
                    'payroll_dates_sum'=>$value->payroll_dates_sum,
                    'status'=>'Open',
                ];
            }
            
        }

        return json_encode($creds,true);

    }

    public function storeCompanyFile(Request $request){
        // echo "<pre>"; print_r(CompanyFile::where('id',1)->first()); echo "</pre>"; die('anil testing');
        
        $params = $request->getParsedBody();
        if (isset($params['zip'])) {
            $company_data = CompanyFile::where('id',$params['zip'])->first();
            $base64String = json_decode($company_data->zip,true)[0];
            $binaryString = base64_decode($base64String);
            header('Content-Type: application/zip');
            header('Content-Disposition: attachment; filename="'.str_replace(' ', '_', $company_data->company_name.'_'.$company_data->date).'file.zip"');
            echo $binaryString;
            exit;
        }
        if (isset($params['screen'])) {
            $company_data = CompanyFile::where('id',$params['screen'])->first();
            $base64ImageData = json_decode($company_data->screen,true)[0];
            // Convert the base64 data to binary
            $imageData = base64_decode($base64ImageData);
            // Set the response headers for downloading
            header("Content-type: image/png");

            header("Content-Disposition: attachment; filename=".str_replace(' ', '_', $company_data->company_name.'_'.$company_data->date)."image.png");
            // Output the image data
            echo $imageData;
            exit;
        }
        // die('anil testing');
        if($params['key'] !== '6$cd[ZU9Gq$:!a[4'){
            return json_encode([
                'success' => false,
                'message' => 'Unauthorized.',
                'data'=>$params
            ],401);
        }
        $status = false;
        if (isset($params['data'])) {
            $data = $params['data'];
            // $data = json_decode($params['data'],true);
            foreach ($data as $key => $value) {
                $company_file = CompanyFile::where('date',date('Y-m-d'))->where('company_name',$value['Company_name'])->where('company_id',$value['id'])->first();
                $company_data = Test::where('id',$value['id'])->first();
                if ($company_data) {
                    $company_data->status = 'Completed';
                    $company_data->save();
                }
                if (empty($company_file)) {
                    $idss = 1;
                    $ids= CompanyFile::orderBy('id','DESC')->first();
                    if ($ids) {
                        $idss = $ids->id + 1;
                    }
                    $company_file = new CompanyFile();
                    $company_file->id = $idss;
                    $company_file->company_id = $value['id'];
                    $company_file->company_name = $value['Company_name'];
                }
                $company_file->date = date('Y-m-d');
                $company_file->zip = json_encode($value['zip_files'],true) ;
                $company_file->screen = json_encode($value['screenshot'],true) ;
                $company_file->save();
                if ($company_file) {
                    $status = true;
                }
            }
        }else{
            return json_encode([
                'success' => false,
                'message' => 'No data found.',
                'data'=>$params
            ],401);
        }
        if ($status) {
            return json_encode([
                'success' => true,
                'message' => 'success.',
                // 'data'=>$data
            ],200);
        }else{
            return json_encode([
                'success' => false,
                'message' => 'No data found.',
                'data'=>$params
            ],401);
        }
    }


    public function getList(Request $request, Response $response, array $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

       

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $sprunje = $classMapper->createInstance('test_sprunje', $classMapper, $params);
        
        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Renders the modal form to confirm user deletion.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException   If user is not found
     * @throws ForbiddenException  If user is not authorized to access page
     * @throws BadRequestException
     */
    public function getModalConfirmDelete(Request $request, Response $response, array $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $classMapper = $this->ci->classMapper;

        // Get the user to delete
        $user = $classMapper->getClassMapping('test')::where('id', $params['user_name'])
            ->first();
        // $user = $this->getUserFromParams($params);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        

        return $this->ci->view->render($response, 'modals/confirm-delete-company.html.twig', [
            'user' => $user,
            'form' => [
                'action' => "api/company/u/{$user->id}",
            ],
        ]);
    }

    /**
     * Renders the modal form for creating a new user.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * If the currently logged-in user has permission to modify user group membership, then the group toggle will be displayed.
     * Otherwise, the user will be added to the default group and receive the default roles automatically.
     *
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getModalCreate(Request $request, Response $response, array $args)
    {
        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var \UserFrosting\I18n\Translator $translator */
        $translator = $this->ci->translator;

        

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        // Determine form fields to hide/disable
        // TODO: come back to this when we finish implementing theming
        $fields = [
            'hidden'   => ['theme'],
            'disabled' => [],
        ];

        // Get a list of all locales
        $locales = $this->ci->locale->getAvailableOptions();

        

        // Hide the locale field if there is only 1 locale available
        if (count($locales) <= 1) {
            $fields['hidden'][] = 'locale';
        }

        // Create a dummy user to prepopulate fields
        $data = [
            'group_id' => $currentUser->group_id,
            'locale'   => $config['site.registration.user_defaults.locale'],
            'theme'    => '',
        ];

        $test = $classMapper->createInstance('test', $data);

        // Load validation rules
        $schema = new RequestSchema('schema://requests/test/create.yaml');
        
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/test.html.twig', [
            'test'    => $test,
            'groups'  => $groups,
            'locales' => $locales,
            'form'    => [
                'action'      => 'api/company',
                'method'      => 'POST',
                'fields'      => $fields,
                'submit_text' => $translator->translate('CREATE'),
            ],
            'page' => [
                'validators' => $validator->rules('json', false),
            ],
        ]);
    }

    /**
     * Renders the modal form for editing an existing user.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the modal, which can be embedded in other pages.
     * This page requires authentication.
     *
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getModalEdit(Request $request, Response $response, array $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;
        // $test = $this->getUserFromParams($params);

        $test = $classMapper->getClassMapping('test')::where('id', $params['user_name'])
            ->first();

        // If the user doesn't exist, return 404
        if (!$test) {
            throw new NotFoundException();
        }


        

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\UserInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var \UserFrosting\I18n\Translator $translator */
        $translator = $this->ci->translator;

        // Access-controlled resource - check that currentUser has permission to edit basic fields "name", "slug", "description" for this role
        $fieldNames = ['name', 'xero_url', 'email','password','secret'];


        // Generate form
        $fields = [
            'hidden'   => [],
            'disabled' => [],
        ];

        // Load validation rules
        $schema = new RequestSchema('schema://requests/test/edit-info.yaml');
        $validator = new JqueryValidationAdapter($schema, $translator);
        $company_name = json_decode($test->name,true);
        $html = '';
        foreach ($company_name as $key => $company) {
            if($company){
            $html .= 
                '
                    <div class="name-field">
                        <div class="col-sm-10" >
                            <div class="form-group">
                                <label>COMPANY NAME</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fas fa-edit fa-fw"></i></span>
                                    <input type="text" class="form-control" name="name[]"  value="'.$company.'" placeholder="" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2" style="margin-top: 24px;" >
                            <div class="form-group">
                                <button type="button" class="deleteNameField btn btn-danger">Delete</button>
                            </div>
                        </div>
                    </div>
                ';
            }
        }
        return $this->ci->view->render($response, 'modals/test.html.twig', [
            'user'    => $test,
            'html'    => $html,
            'form'    => [
                // 'action'      => "api/company/u/".$test->id,
                // 'method'      => 'PUT',
                // 'fields'      => $fields,
                'submit_text' => $translator->translate('UPDATE'),
                'action'      => 'api/company',
                'method'      => 'POST',
                'fields'      => $fields,
            ],
            'page' => [
                'validators' => $validator->rules('json', false),
            ],
        ]);
    }

    
    public function getModalFile(Request $request, Response $response, array $args)
    {
        // GET parameters
        $params = $request->getQueryParams();
        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;
        // $test = $this->getUserFromParams($params);
        $test = $classMapper->getClassMapping('test')::where('id', $params['user_name'])
            ->first();
        // If the user doesn't exist, return 404
        if (!$test) {
            throw new NotFoundException();
        }
        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;
        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\UserInterface $currentUser */
        $currentUser = $this->ci->currentUser;
        /** @var \UserFrosting\I18n\Translator $translator */
        $translator = $this->ci->translator;
        // Access-controlled resource - check that currentUser has permission to edit basic fields "name", "slug", "description" for this role

        // Load validation rules
        $schema = new RequestSchema('schema://requests/test/edit-info.yaml');
        $validator = new JqueryValidationAdapter($schema, $translator);
        $company_name = CompanyFile::where('company_id',$test->id)->where('date','>=',date('Y-m-d',strtotime('-3 days')))->orderBy('id','DESC')->get();
        $html = '';
        foreach ($company_name as $key => $company) {
            if($company){
                $zip = base64_decode(json_decode($company->zip,true)[0]);
                $screen = json_decode($company->screen,true)[0];
            $html .= 
                '
                    <tr>
                        <td scope="col">'.$company->company_name.'</td>
                        <td scope="col">'.$company->date.'</td>
                        <td scope="col">
                            <form action="/api/company/file" method="post">
                                <input type="hidden" name="zip" value="'.$company->id.'">
                                <button class="btn-info" type="submit">Download Zip</button>
                            </form>
                        </td>
                        <td scope="col">
                            <form action="/api/company/file" method="post">
                                <input type="hidden" name="screen" value="'.$company->id.'">
                                <button class="btn-info" type="submit">Download Image</button>
                            </form>
                        </td>
                    </tr>
                ';
            }
        }
        // <td scope="col">
        //                     <a href="data:image/png;base64,'.$screen.'" target="_blank">
        //                         <i class="fa fa-download" aria-hidden="true"></i>
        //                     </a>
        //                 </td>
        return $this->ci->view->render($response, 'modals/company-file.html.twig', [
            'user'    => $test,
            'html'    => $html,
            'form'    => [
                // 'action'      => "api/company/u/".$test->id,
                // 'method'      => 'PUT',
                // 'fields'      => $fields,
                'submit_text' => $translator->translate('UPDATE'),
                'action'      => 'api/company',
                'method'      => 'POST',
                'fields'      => $fields,
            ],
            'page' => [
                'validators' => $validator->rules('json', false),
            ],
        ]);
    }

    public function downloadCompanyFile(){
        echo "<pre>"; print_r($_REQUEST); echo "</pre>"; die('anil testing');
    }

    /**
     * Renders the modal form for editing a user's password.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the form, which can be embedded in other pages.
     * This page requires authentication.
     *
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getModalEditPassword(Request $request, Response $response, array $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $user = $this->getUserFromParams($params);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        

        // Load validation rules
        $schema = new RequestSchema('schema://requests/user/edit-password.yaml');
        $schema->set('password.validators.length.min', $config['site.password.length.min']);
        $schema->set('password.validators.length.max', $config['site.password.length.max']);
        $schema->set('passwordc.validators.length.min', $config['site.password.length.min']);
        $schema->set('passwordc.validators.length.max', $config['site.password.length.max']);
        $validator = new JqueryValidationAdapter($schema, $this->ci->translator);

        return $this->ci->view->render($response, 'modals/user-set-password.html.twig', [
            'user' => $user,
            'page' => [
                'validators' => $validator->rules('json', false),
            ],
        ]);
    }

    /**
     * Renders the modal form for editing a user's roles.
     *
     * This does NOT render a complete page.  Instead, it renders the HTML for the form, which can be embedded in other pages.
     * This page requires authentication.
     *
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getModalEditRoles(Request $request, Response $response, array $args)
    {
        // GET parameters
        $params = $request->getQueryParams();

        $user = $this->getUserFromParams($params);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled resource - check that currentUser has permission to edit "roles" field for this user
        if (!$authorizer->checkAccess($currentUser, 'update_user_field', [
            'user'   => $user,
            'fields' => ['roles'],
        ])) {
            throw new ForbiddenException();
        }

        return $this->ci->view->render($response, 'modals/user-manage-roles.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * Returns a list of effective Permissions for a specified User.
     *
     * Generates a list of permissions, optionally paginated, sorted and/or filtered.
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getPermissions(Request $request, Response $response, array $args)
    {
        $user = $this->getUserFromParams($args);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        // GET parameters
        $params = $request->getQueryParams();

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'view_user_field', [
            'user'     => $user,
            'property' => 'permissions',
        ])) {
            throw new ForbiddenException();
        }

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        $params['user_id'] = $user->id;
        $sprunje = $classMapper->createInstance('user_permission_sprunje', $classMapper, $params);

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Returns roles associated with a single user.
     *
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function getRoles(Request $request, Response $response, array $args)
    {
        $user = $this->getUserFromParams($args);

        // If the user doesn't exist, return 404
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // GET parameters
        $params = $request->getQueryParams();

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'view_user_field', [
            'user'     => $user,
            'property' => 'roles',
        ])) {
            throw new ForbiddenException();
        }

        $sprunje = $classMapper->createInstance('role_sprunje', $classMapper, $params);
        $sprunje->extendQuery(function ($query) use ($user) {
            return $query->forUser($user->id);
        });

        // Be careful how you consume this data - it has not been escaped and contains untrusted user-supplied content.
        // For example, if you plan to insert it into an HTML DOM, you must escape it on the client side (or use client-side templating).
        return $sprunje->toResponse($response);
    }

    /**
     * Renders a page displaying a user's information, in read-only mode.
     *
     * This checks that the currently logged-in user has permission to view the requested user's info.
     * It checks each field individually, showing only those that you have permission to view.
     * This will also try to show buttons for activating, disabling/enabling, deleting, and editing the user.
     *
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function pageInfo(Request $request, Response $response, array $args)
    {
        $user = $this->getUserFromParams($args);

        // If the user no longer exists, forward to main user listing page
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        // Access-controlled page
        if (!$authorizer->checkAccess($currentUser, 'uri_user', [
            'user' => $user,
        ])) {
            throw new ForbiddenException();
        }

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        // Get a list of all locales
        $locales = $this->ci->locale->getAvailableOptions();

        // Determine fields that currentUser is authorized to view
        $fieldNames = ['user_name', 'name', 'email', 'locale', 'group', 'roles'];

        // Generate form
        $fields = [
            // Always hide these
            'hidden' => ['theme'],
        ];

        // Determine which fields should be hidden
        foreach ($fieldNames as $field) {
            if (!$authorizer->checkAccess($currentUser, 'view_user_field', [
                'user'     => $user,
                'property' => $field,
            ])) {
                $fields['hidden'][] = $field;
            }
        }

        // Hide the locale field if there is only 1 locale available
        if (count($locales) <= 1) {
            $fields['hidden'][] = 'locale';
        }

        // Determine buttons to display
        $editButtons = [
            'hidden' => [],
        ];

        if (!$authorizer->checkAccess($currentUser, 'update_user_field', [
            'user'   => $user,
            'fields' => ['name', 'email', 'locale'],
        ])) {
            $editButtons['hidden'][] = 'edit';
        }

        if (!$authorizer->checkAccess($currentUser, 'update_user_field', [
            'user'   => $user,
            'fields' => ['flag_enabled'],
        ])) {
            $editButtons['hidden'][] = 'enable';
        }

        if (!$authorizer->checkAccess($currentUser, 'update_user_field', [
            'user'   => $user,
            'fields' => ['flag_verified'],
        ])) {
            $editButtons['hidden'][] = 'activate';
        }

        if (!$authorizer->checkAccess($currentUser, 'update_user_field', [
            'user'   => $user,
            'fields' => ['password'],
        ])) {
            $editButtons['hidden'][] = 'password';
        }

        if (!$authorizer->checkAccess($currentUser, 'update_user_field', [
            'user'   => $user,
            'fields' => ['roles'],
        ])) {
            $editButtons['hidden'][] = 'roles';
        }

        if (!$authorizer->checkAccess($currentUser, 'delete_user', [
            'user' => $user,
        ])) {
            $editButtons['hidden'][] = 'delete';
        }

        // Determine widgets to display
        $widgets = [
            'hidden' => [],
        ];

        if (!$authorizer->checkAccess($currentUser, 'view_user_field', [
            'user'     => $user,
            'property' => 'permissions',
        ])) {
            $widgets['hidden'][] = 'permissions';
        }

        if (!$authorizer->checkAccess($currentUser, 'view_user_field', [
            'user'     => $user,
            'property' => 'activities',
        ])) {
            $widgets['hidden'][] = 'activities';
        }

        return $this->ci->view->render($response, 'pages/user.html.twig', [
            'user'            => $user,
            'locales'         => $locales,
            'fields'          => $fields,
            'tools'           => $editButtons,
            'widgets'         => $widgets,
            'delete_redirect' => $this->ci->router->pathFor('uri_users'),
        ]);
    }

    /**
     * Renders the user listing page.
     *
     * This page renders a table of users, with dropdown menus for admin actions for each user.
     * Actions typically include: edit user details, activate user, enable/disable user, delete user.
     *
     * This page requires authentication.
     * Request type: GET
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function pageList(Request $request, Response $response, array $args)
    {
        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        

        return $this->ci->view->render($response, 'pages/companys.html.twig');
    }

    /**
     * Processes the request to update an existing user's basic details (first_name, last_name, email, locale, group_id).
     *
     * Processes the request from the user update form, checking that:
     * 1. The target user's new email address, if specified, is not already in use;
     * 2. The logged-in user has the necessary permissions to update the putted field(s);
     * 3. The submitted data is valid.
     *
     * This route requires authentication.
     * Request type: PUT
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException  If user is not found
     * @throws ForbiddenException If user is not authorized to access page
     */
    public function updateInfo(Request $request, Response $response, $id)
    {
        $classMapper = $this->ci->classMapper;

        // Get the user to delete
        $user = $classMapper->getClassMapping('test')::where('id', $id)
            ->first();
        // $user = $this->getUserFromParams($args);
        echo "<pre>"; print_r($user); echo "</pre>"; die('anil testing');
        if (!$user) {
            throw new NotFoundException();
        }

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        // Get PUT parameters
        $params = $request->getParsedBody();

        /** @var \UserFrosting\Sprinkle\Core\Alert\AlertStream $ms */
        $ms = $this->ci->alerts;

        // Load the request schema
        $schema = new RequestSchema('schema://requests/test/edit-info.yaml');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        $error = false;

        // Validate request data
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            $ms->addValidationErrors($validator);
            $error = true;
        }

        // Determine targeted fields
        $fieldNames = [];
        foreach ($data as $name => $value) {
            if ($name == 'first_name' || $name == 'last_name') {
                $fieldNames[] = 'name';
            } elseif ($name == 'group_id') {
                $fieldNames[] = 'group';
            } else {
                $fieldNames[] = $name;
            }
        }

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        


        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        
        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction(function () use ($data, $user, $currentUser) {
            // Update the user and generate success messages
            foreach ($data as $name => $value) {
                if ($value != $user->$name) {
                    $user->$name = $value;
                }
            }

            $user->save();

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated basic test onfo.", [
                'type'    => 'test_update_info',
                'user_id' => $currentUser->id,
            ]);
        });

        $ms->addMessageTranslated('success', 'COMPANY_UPDATED', [
            'user_name' => $user->name,
        ]);

        return $response->withJson([], 200);
    }

    /**
     * Processes the request to update a specific field for an existing user.
     *
     * Supports editing all user fields, including password, enabled/disabled status and verification status.
     * Processes the request from the user update form, checking that:
     * 1. The logged-in user has the necessary permissions to update the putted field(s);
     * 2. We're not trying to disable the master account;
     * 3. The submitted data is valid.
     *
     * This route requires authentication.
     * Request type: PUT
     *
     * @param Request  $request
     * @param Response $response
     * @param string[] $args
     *
     * @throws NotFoundException   If user is not found
     * @throws ForbiddenException  If user is not authorized to access page
     * @throws BadRequestException
     */
    public function updateField(Request $request, Response $response, array $args)
    {
        // Get the username from the URL
        $user = $this->getUserFromParams($args);

        if (!$user) {
            throw new NotFoundException();
        }

        // Get key->value pair from URL and request body
        $fieldName = $args['field'];

        /** @var \UserFrosting\Sprinkle\Account\Authorize\AuthorizationManager $authorizer */
        $authorizer = $this->ci->authorizer;

        /** @var \UserFrosting\Sprinkle\Account\Database\Models\Interfaces\TestInterface $currentUser */
        $currentUser = $this->ci->currentUser;

        

        /** @var \UserFrosting\Support\Repository\Repository $config */
        $config = $this->ci->config;

        // Only the master account can edit the master account!
        if (
            ($user->id == $config['reserved_user_ids.master']) &&
            ($currentUser->id != $config['reserved_user_ids.master'])
        ) {
            throw new ForbiddenException();
        }

        // Get PUT parameters: value
        $put = $request->getParsedBody();

        // Make sure data is part of $_PUT data, default to empty value if sensible, otherwise error
        if (isset($put[$fieldName])) {
            $fieldData = $put[$fieldName];
        } else {
            if ($fieldName == 'roles') {
                $fieldData = [];
            } else {
                throw new BadRequestException();
            }
        }

        // Create and validate key -> value pair
        $params = [
            $fieldName => $fieldData,
        ];

        // Load the request schema
        $schema = new RequestSchema('schema://requests/user/edit-field.yaml');
        $schema->set('password.validators.length.min', $config['site.password.length.min']);
        $schema->set('password.validators.length.max', $config['site.password.length.max']);

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        // Validate, and throw exception on validation errors.
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            // TODO: encapsulate the communication of error messages from ServerSideValidator to the BadRequestException
            $e = new BadRequestException();
            foreach ($validator->errors() as $idx => $field) {
                foreach ($field as $eidx => $error) {
                    $e->addUserMessage($error);
                }
            }

            throw $e;
        }

        // Get validated and transformed value
        $fieldValue = $data[$fieldName];

        /** @var \UserFrosting\Sprinkle\Core\Alert\AlertStream $ms */
        $ms = $this->ci->alerts;

        // Special checks and transformations for certain fields
        if ($fieldName == 'flag_enabled') {
            // Check that we are not disabling the master account
            if (
                ($user->id == $config['reserved_user_ids.master']) &&
                ($fieldValue == '0')
            ) {
                $e = new BadRequestException();
                $e->addUserMessage('DISABLE_MASTER');

                throw $e;
            } elseif (
                ($user->id == $currentUser->id) &&
                ($fieldValue == '0')
            ) {
                $e = new BadRequestException();
                $e->addUserMessage('DISABLE_SELF');

                throw $e;
            }
        } elseif ($fieldName == 'password') {
            $fieldValue = Password::hash($fieldValue);
        }

        // Begin transaction - DB will be rolled back if an exception occurs
        Capsule::transaction(function () use ($fieldName, $fieldValue, $user, $currentUser) {
            if ($fieldName == 'roles') {
                $newRoles = collect($fieldValue)->pluck('role_id')->all();
                $user->roles()->sync($newRoles);
            } else {
                $user->$fieldName = $fieldValue;
                $user->save();
            }

            // Create activity record
            $this->ci->userActivityLogger->info("User {$currentUser->user_name} updated property '$fieldName' for user {$user->user_name}.", [
                'type'    => 'account_update_field',
                'user_id' => $currentUser->id,
            ]);
        });

        // Add success messages
        if ($fieldName == 'flag_enabled') {
            if ($fieldValue == '1') {
                $ms->addMessageTranslated('success', 'ENABLE_SUCCESSFUL', [
                    'user_name' => $user->user_name,
                ]);
            } else {
                $ms->addMessageTranslated('success', 'DISABLE_SUCCESSFUL', [
                    'user_name' => $user->user_name,
                ]);
            }
        } elseif ($fieldName == 'flag_verified') {
            $ms->addMessageTranslated('success', 'MANUALLY_ACTIVATED', [
                'user_name' => $user->user_name,
            ]);
        } else {
            $ms->addMessageTranslated('success', 'COMPANY_UPDATED', [
                'user_name' => $user->user_name,
            ]);
        }

        return $response->withJson([], 200);
    }

    /**
     * Get User instance from params.
     *
     * @param string[] $params
     *
     * @throws BadRequestException
     *
     * @return User|null
     */
    protected function getUserFromParams(array $params): ?User
    {
        // Load the request schema
        $schema = new RequestSchema('schema://requests/user/get-by-username.yaml');

        // Whitelist and set parameter defaults
        $transformer = new RequestDataTransformer($schema);
        $data = $transformer->transform($params);

        // Validate, and throw exception on validation errors.
        $validator = new ServerSideValidator($schema, $this->ci->translator);
        if (!$validator->validate($data)) {
            // TODO: encapsulate the communication of error messages from ServerSideValidator to the BadRequestException
            $e = new BadRequestException();
            foreach ($validator->errors() as $idx => $field) {
                foreach ($field as $eidx => $error) {
                    $e->addUserMessage($error);
                }
            }

            throw $e;
        }

        /** @var \UserFrosting\Sprinkle\Core\Util\ClassMapper $classMapper */
        $classMapper = $this->ci->classMapper;

        // Get the user to delete
        $user = $classMapper->getClassMapping('test')::where('id', $data['user_name'])
            ->first();
        // echo "<pre>"; print_r($user); echo "</pre>"; die('anil testing');
        return $user;
    }
}
