<?php

/*
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @copyright Copyright (c) 2019 Alexander Weissman
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/LICENSE.md (MIT License)
 */

namespace UserFrosting\Sprinkle\Account\Database\Migrations\v400;

use Illuminate\Database\Schema\Blueprint;
use UserFrosting\Sprinkle\Core\Database\Migration;

/**
 * Users table migration
 * Removed the 'display_name', 'title', 'secret_token', and 'flag_password_reset' fields, and added first and last name and 'last_activity_id'.
 * Version 4.0.0.
 *
 * See https://laravel.com/docs/5.8/migrations#tables
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class TestsTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('company')) {
            $this->schema->create('company', function (Blueprint $table) {
                $table->increments('id');
                $table->string('xero_url', 255)->nullable();
                $table->string('email', 255)->nullable();
                $table->string('password', 255)->nullable();
                $table->string('secret', 255)->nullable();
                $table->string('q1', 255)->nullable();
                $table->string('q2', 255)->nullable();
                $table->string('q3', 255)->nullable();
                $table->string('q4', 255)->nullable();
                $table->string('q5', 255)->nullable();
                $table->string('name', 255)->nullable();
                $table->string('dates_one_yr', 255)->nullable();
                $table->string('date1', 255)->nullable();
                $table->string('date2', 255)->nullable();
                $table->string('date3', 255)->nullable();
                $table->string('date_range1_start', 255)->nullable();
                $table->string('date_range1_end', 255)->nullable();
                $table->string('date_range2_start', 255)->nullable();
                $table->string('date_range2_end', 255)->nullable();
                $table->string('date_range3_start', 255)->nullable();
                $table->string('date_range3_end', 255)->nullable();
                $table->string('start_long_date', 255)->nullable();
                $table->string('end_long_date', 255)->nullable();
                $table->string('start_date', 255)->nullable();
                $table->string('end_date', 255)->nullable();
                $table->string('payroll_dates_act1', 255)->nullable();
                $table->string('payroll_dates_act2', 255)->nullable();
                $table->string('payroll_dates_sum1', 255)->nullable();
                $table->string('payroll_dates_sum2', 255)->nullable();
                $table->string('status', 255)->default('New');
                $table->softDeletes();
                $table->timestamps();

               
            });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->schema->drop('company');
    }
}
