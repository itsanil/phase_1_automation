<?php

/*
 * UserFrosting (http://www.userfrosting.com)
 *
 * @link      https://github.com/userfrosting/UserFrosting
 * @copyright Copyright (c) 2019 Alexander Weissman
 * @license   https://github.com/userfrosting/UserFrosting/blob/master/LICENSE.md (MIT License)
 */

namespace UserFrosting\Sprinkle\Account\Database\Migrations\v400;

use Illuminate\Database\Schema\Blueprint;
use UserFrosting\Sprinkle\Core\Database\Migration;

/**
 * Users table migration
 * Removed the 'display_name', 'title', 'secret_token', and 'flag_password_reset' fields, and added first and last name and 'last_activity_id'.
 * Version 4.0.0.
 *
 * See https://laravel.com/docs/5.8/migrations#tables
 *
 * @author Alex Weissman (https://alexanderweissman.com)
 */
class CompanyFilesTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        if (!$this->schema->hasTable('company_files')) {
            $this->schema->create('company_files', function (Blueprint $table) {
                $table->increments('id');
                $table->string('company_id', 255)->nullable();
                $table->string('company_name', 255)->nullable();
                $table->string('date', 255)->nullable();
                $table->longText('zip', 255)->nullable();
                $table->longText('screen', 255)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->schema->drop('company_files');
    }
}
